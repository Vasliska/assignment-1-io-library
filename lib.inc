%define SYSTEM_EXIT 60
%define SYSTEM_READ 0
%define SYSTEM_WRITE 1
%define STDIN 0
%define STDOUT 1

%define TAB       9 ; 0x9
%define NEW_LINE '\n' ; 0xA
%define SPACE    ' ' ; 0x20
%define C_RETURN 13

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSTEM_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	
	.forward_iterate:
		cmp byte[rdi + rax], 0
		je .end
		inc rax
		jmp .forward_iterate
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi

	mov rsi, rdi		
	mov rdx, rax		
	mov rax, SYSTEM_WRITE	
	mov rdi, STDOUT		
	syscall 		

	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, NEW_LINE


; Принимает код символа и выводит его в stdout
print_char:
  	xor rax, rax
	push rdi
	mov rsi, rsp		
	mov rdx, 1 		
	mov rax, SYSTEM_WRITE 	
	mov rdi, STDOUT 	
	syscall 		
	pop rdi
	ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    push r12
	push r13
	mov r12,rsp 
	mov r13,10 
	mov rax,rdi 
	dec rsp 
	mov byte[rsp],0 
	.loop:
		dec rsp 
		xor rdx,rdx 
		div r13 
		add rdx,0x30
		mov byte[rsp],dl 
		test rax,rax 
		jz .print 
		jmp .loop ;
	.print:
		mov rdi,rsp 
		call print_string 
		mov rsp,r12 
	pop r13 ;
	pop r12 ;
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
  	xor rax,rax 
	mov rax,rdi 
	test rax,rax 
	jns .pos 
	mov rdi,'-' 
	push rax
	call print_char 
	pop rax 
	neg rax 
	mov rdi,rax 
	.pos: 
		call print_uint 
		ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
   	call string_length 
	mov rcx,rax 
	xchg rdi, rsi 
	call string_length
	cmp rax,rcx 
	jne .not_equals 
	repe cmpsb 
	jne .not_equals 
	mov rax,1 
	ret
	.not_equals:
		mov rax,0 
		ret	
		
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char: ; поменяла, потому что показалось такое решение более лаконичное 
	push 0
	mov rax, SYSTEM_READ	
	mov rdi, STDIN
	mov rsi, rsp		
	mov rdx, 1	
	syscall	
	xor rax, rax	
	pop rax	
	ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word: ;буду честна - посмотрела чужие репы и поняла как лучше его сделать нежели чем у меня было
	xor rax, rax
	xor r8, r8
	mov r9, rsi
	
	dec r9
	.next:
		push rdi
		call read_char		
		pop rdi

		cmp al, SPACE	
		je .next
		cmp al, NEW_LINE
		je .next
		cmp al, TAB
		je .next
		cmp al, C_RETURN
		je .next
		test al, al
		jz .correct_ending

	.again:
		mov byte[rdi + r8], al
		inc r8
		push rdi
		call read_char		
		pop rdi
		cmp al, SPACE	
		je .correct_ending
		cmp al, NEW_LINE	
		je .correct_ending
		cmp al, TAB		
		je .correct_ending
		cmp al, C_RETURN
		je .correct_ending

		test al, al		
		jz .correct_ending
		cmp r9, r8		
		je .overflow

		jmp .again

	.correct_ending:
		mov byte[rdi + r8], 0
		mov rax, rdi		
		mov rdx, r8
		ret

	.overflow:
		mov rax, 0
        mov rdx, 0
		ret
	

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	call string_length 
	mov rcx,rax 
	mov rsi,rdi
	xor rdx,rdx 
	xor rax,rax 
	.pars: 
		xor rdi,rdi 
		mov dil,byte[rsi+rdx] 
		cmp dil,'0' 
		jb .end ;
		cmp dil,'9' 
		ja .end ;
		sub dil,'0' 
		imul rax,10 
		add rax,rdi 
		inc rdx 
		dec rcx 
		jnz .pars 
	.end:
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
 	 cmp byte[rdi],'-' 
	je .minus 
	jmp parse_uint 
	.minus:
		inc rdi 
		call parse_uint 
		test rdx,rdx 
		jz .null 
		neg rax 
		inc rdx 
		ret
	.null:
		xor rax,rax
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  	xor rax, rax ;do
	xor r9, r9 
	xor rcx, rcx 
    call string_length 
	push rax
    push rsi 
	.loop:
    		cmp rcx, rdx 
		je .error
    		mov r8, [rdi+rcx] 
    		mov [rsi+rcx], r8 
    		cmp rax, 0 
    		je .end 
    		dec rax 
    		inc rcx 
    		jmp .loop 
	.end
		pop rsi 
		pop rax
    	mov byte [rsi+rax], 0 
    	ret
	.error:
    	pop rsi 
		pop rax
		mov rax, 0 
		ret
